Simple calculator for Linux
===

Prerequisite:
- Perl

Usage:

**Interactive Mode**

    calc

**Standard Input Mode**

    calc -

**Commandline Argument**

    calc -e "expressions"

**Read from file**
    
    calc "filename"

Multiple expressions can be separated by , ; or newline.

To refer to the previous evaluation result, use _

For example,

    3 * 100
    =>300
    
    _ / 2
    =>150

There is also a small command line argument parser you can use in other situations.
